var toggler = document.getElementsByClassName("box");
var clients = [];
var projects = [];

$('#add_user_modal').on('show.bs.modal', function (event) {
	clients = [];
});

$('#update_user_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var email = button.data('email');
	var password = '********';
	var role = button.data('role');
	var id_client = button.data('id_client');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#inputName').val(name);
	modal.find('#inputEmail').val(email);
	modal.find('#inputPassword').val(password);
	modal.find('#role').val(role);
	modal.find('#client').val(id_client);
	clients = [];
});

$('#delete_user_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#message').text("Sei sicuro di voler rimuovere l'utente " + name + "?");
});

for (var i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
	var id = jQuery(this).data('id');
	var type = jQuery(this).data('type');
    var nested = this.parentElement.querySelector(".nested");
    if (nested != null) {
    	nested.classList.toggle("active");
    }
    if (type == "client") {
	    this.classList.toggle("check-box");
		if(jQuery(this).hasClass("check-box")){
			clients.push(id);
		}
		else {
			for(var j = 0; j < clients.length; j++) {
				if (clients[j] === id) {
					clients.splice(j, 1);
				}
			}
		}
		var clients_string = "";
		for(var j = 0; j < clients.length; j++) {
			clients_string += clients[j];
			if (j < clients.length - 1) {
				clients_string += "-";
			}
		}
		$("#input_clients_ids_add_user_modal").val(clients_string);
		$("#input_clients_ids_update_user_modal").val(clients_string);
    } else if (type == "project") {
	    this.classList.toggle("check-box");
		if(jQuery(this).hasClass("check-box")){
			projects.push(id);
		}
		else {
			for(var j = 0; j < projects.length; j++) {
				if (projects[j] === id) {
					projects.splice(j, 1);
				}
			}
		}
		var projects_string = "";
		for(var j = 0; j < projects.length; j++) {
			projects_string += projects[j];
			if (j < projects.length - 1) {
				projects_string += "-";
			}
		}
		$("#input_projects_ids_add_user_modal").val(projects_string);
		$("#input_projects_ids_update_user_modal").val(projects_string);
    }
  });
}
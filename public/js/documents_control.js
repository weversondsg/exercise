$('#update_document_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#inputName').val(name);
});

$('#delete_document_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#message').text("Sei sicuro di voler rimuovere il progetto " + name + "?");
});

$('#update_status_document_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#document_name').text(name);
});

$('.clickable_item').click(function() {
	var name = jQuery(this).data('name');
	window.location.href = "/uploads/" + name;
});
$('#update_client_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#inputName').val(name);
});

$('#delete_client_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var id = button.data('id');
	var name = button.data('name');
	var modal = $(this);
	modal.find('#inputId').val(id);
	modal.find('#message').text("Sei sicuro di voler rimuovere il cliente " + name + "?");
});

$('.clickable_item').click(function() {
	var id = jQuery(this).data('id');
	window.location.href = "/projects_control?id_client=" + id;
});
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('validate_login', ['uses' =>'Controller@validate_login']);

Route::get('/', 'Controller@login');
Route::get('home', 'Controller@home');
Route::get('login', 'Controller@login');
Route::post('validate_login', 'Controller@validate_login');

Route::get('users_control', 'Controller@users_control');
Route::post('add_user', 'Controller@add_user');
Route::post('update_user', 'Controller@update_user');
Route::post('delete_user', 'Controller@delete_user');

Route::get('clients_control', 'Controller@clients_control');
Route::post('add_client', 'Controller@add_client');
Route::post('update_client', 'Controller@update_client');
Route::post('delete_client', 'Controller@delete_client');

Route::get('projects_control', 'Controller@projects_control');
Route::post('add_project', 'Controller@add_project');
Route::post('update_project', 'Controller@update_project');
Route::post('delete_project', 'Controller@delete_project');

Route::get('documents_control', 'Controller@documents_control');
Route::post('add_document', 'Controller@add_document');
Route::post('update_document', 'Controller@update_document');
Route::post('delete_document', 'Controller@delete_document');
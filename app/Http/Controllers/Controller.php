<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Hash;
use Auth;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function users_control() {
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}
    	
    	$clients = DB::table('clients')->get();

    	$users = DB::table('users')->get();

    	return view('users_control', ['users' => $users, 'clients' => $clients]);
    }

    public function add_user(Request $request)
    {
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}
		
    	$name = $request->post('name');
    	$email = $request->post('email');
    	$password = $request->post('password');
    	$is_admin = $request->post('is_admin');

    	$id_user = DB::table('users')->insertGetId(
		    ['name' => $name, 'email' => $email, 'password' => Hash::make($password), 'is_admin' => $is_admin]
		);

    	$clients_ids = explode("-", $request->post('clients'));

		foreach ($clients_ids as &$id_client) {
			DB::table('rel_users_clients')->insert(
			    ['id_user' => $id_user, 'id_client' => $id_client]
			);
		}

    	$projects_ids = explode("-", $request->post('projects'));

		foreach ($projects_ids as &$id_project) {
			DB::table('rel_users_projects')->insert(
			    ['id_user' => $id_user, 'id_project' => $id_project]
			);
		}

    	return redirect('users_control');
    }

    public function update_user(Request $request)
    {
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$id = $request->post('id');
    	$name = $request->post('name');
    	$email = $request->post('email');
    	$password = $request->post('password');
    	$is_admin = $request->post('is_admin');

    	if ($password == '********') {
    		DB::table('users')
            ->where('id', $id)
            ->update(['name' => $name, 'email' => $email, 'is_admin' => $is_admin]);
    	} else {
    		DB::table('users')
            ->where('id', $id)
            ->update(['name' => $name, 'email' => $email, 'password' => Hash::make($password), 'is_admin' => $is_admin]);
    	}

    	$clients_ids = explode("-", $request->post('clients'));

		DB::table('rel_users_clients')->where('id_user', $id)->delete();
		foreach ($clients_ids as &$id_client) {
			DB::table('rel_users_clients')->insert(
			    ['id_user' => $id, 'id_client' => $id_client]
			);
		}

    	$projects_ids = explode("-", $request->post('projects'));

		DB::table('rel_users_projects')->where('id_user', $id)->delete();
		foreach ($projects_ids as &$id_project) {
			DB::table('rel_users_projects')->insert(
			    ['id_user' => $id, 'id_project' => $id_project]
			);
		}

    	return redirect('users_control');
    }

    public function delete_user(Request $request)
    {
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$id = $request->post('id');

    	DB::table('rel_users_clients')->where('id_user', $id)->delete();
    	DB::table('users')->where('id', $id)->delete();

    	return redirect('users_control');
    }

    public function clients_control() {
    	if (!Auth::check()) {
			return abort(401);
		}
    	
    	if (Auth::user()->is_admin) {
			$clients = DB::table('clients')
	    		->get();
		} else {
	    	$clients = DB::table('clients')
	    		->join('rel_users_clients', 'clients.id', '=', 'rel_users_clients.id_client')
		    	->select('clients.*')
				->where('rel_users_clients.id_user', Auth::user()->id)
	    		->get();
		}

    	return view('clients_control', ['clients' => $clients]);
    }

    public function add_client(Request $request)
    {
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$name = $request->post('name');
    	DB::table('clients')->insert(
		    ['name' => $name]
		);

    	return redirect('clients_control');
    }

	public function update_client(Request $request)
	{
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

		$id = $request->post('id');
		$name = $request->post('name');

		DB::table('clients')
		->where('id', $id)
		->update(['name' => $name]);

		return redirect('clients_control');
	}

    public function delete_client(Request $request)
    {
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$id = $request->post('id');

    	DB::table('rel_users_clients')->where('id_client', $id)->delete();
    	DB::table('projects')->where('id_client', $id)->delete();
    	DB::table('clients')->where('id', $id)->delete();

    	return redirect('clients_control');
    }

    public function projects_control(Request $request) {
    	if (!Auth::check()) {
			return abort(401);
		}

		$id_client = $request->get('id_client');

		if ($id_client == null) {
			return abort(401);
		}

		if (Auth::user()->is_admin) {
	    	$projects = DB::table('projects')
		    	->join('clients', 'clients.id', '=', 'projects.id_client')
		    	->select('projects.*', 'clients.name as client_name')
		    	->where('projects.id_client', $id_client)
		    	->get();
		} else {
	    	$projects = DB::table('projects')
	    		->join('rel_users_projects', 'projects.id', '=', 'rel_users_projects.id_project')
		    	->join('clients', 'clients.id', '=', 'projects.id_client')
		    	->select('projects.*', 'clients.name as client_name')
				->where('rel_users_projects.id_user', Auth::user()->id)
		    	->where('projects.id_client', $id_client)
	    		->get();
		}

	    $client = DB::table('clients')
	    	->where('id', $id_client)
	    	->get()
	    	->first();

		if ($client == null) {
			return abort(401);
		}

    	return view('projects_control', ['projects' => $projects, 'client' => $client]);
    }

    public function add_project(Request $request)
    {
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$name = $request->post('name');
    	$id_client = $request->post('id_client');

    	DB::table('projects')->insert(
		    ['name' => $name, 'id_client' => $id_client]
		);

    	return redirect('projects_control?id_client='.$id_client);
    }

	public function update_project(Request $request)
	{
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

		$id = $request->post('id');
		$name = $request->post('name');
    	$id_client = $request->post('id_client');

		DB::table('projects')
		->where('id', $id)
		->update(['name' => $name]);

    	return redirect('projects_control?id_client='.$id_client);
	}

    public function delete_project(Request $request)
    {
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

    	$id = $request->post('id');
    	$id_client = $request->post('id_client');

    	DB::table('projects')->where('id', $id)->delete();

    	return redirect('projects_control?id_client='.$id_client);
    }

    public function documents_control(Request $request) {

    	$id_project = $request->post('id_project');

    	if ($id_project == null || !Auth::check()) {
			return abort(401);
		}
    	
		$documents = DB::table('documents')
			->where('id_project', $id_project)
			->get();

    	return view('documents_control', ['documents' => $documents, 'id_project' => $id_project]);
    }

    public function add_document(Request $request)
    {
		if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

		$id_project = $request->post('id_project');

		$target_dir = "uploads/";
		$filename = basename($_FILES["fileToUpload"]["name"]);
		$target_file = $target_dir . $filename;
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

		if (file_exists($target_file)) {
			return "Spiacenti, già esiste uno file con lo stesso nome.";
		}

		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "Il file ". $filename. " è stato inviato.";
		} else {
			echo "Spiacenti, si è verificato un errore durante il caricamento del file.";
		}

    	DB::table('documents')->insert(
		    ['name' => $filename, 'id_project' => $id_project]
		);

		$documents = DB::table('documents')
			->where('id_project', $id_project)
			->get();

    	return redirect('documents_control?id_project='.$id_project);
    }

	public function update_document(Request $request)
	{
		if (!Auth::check()) {
			return abort(401);
		}

		$id = $request->post('id');
		$status = $request->post('btn');
		$id_project = $request->post('id_project');

		DB::table('documents')
			->where('id', $id)
			->update(['status' => $status]);

    	return redirect('documents_control?id_project='.$id_project);
	}

    public function delete_document(Request $request)
    {
    	if (!Auth::check()) {
			return abort(401);
		}

    	$id = $request->post('id');
    	$id_project = $request->post('id_project');

    	$document = DB::table('documents')->where('id', $id)->get()->first();

    	DB::table('documents')->where('id', $id)->delete();

    	unlink("uploads/".$document->name);

    	return redirect('documents_control?id_project='.$id_project);
    }

	public function login()
	{
		return view('login');
	}

	public function home()
	{
    	if (!Auth::check() || !Auth::user()->is_admin) {
			return abort(401);
		}

		return view('home');
	}

    public function validate_login(Request $request) {

    	$email = $request->post('email');
    	$password = $request->post('password');

    	if (Auth::attempt(['email' => $email, 'password' => $password])) {
    		if (Auth::user()->is_admin) {
    			return redirect('home');
    		} else {
    			return redirect('clients_control');
    		}
		}
		return 'failed';

    }
}

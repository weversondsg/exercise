<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Utenti</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/general.css">
	<link rel="stylesheet" href="/css/users_control.css">
</head>
<body>

<div class="modal fade" id="add_user_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/add_user">
				{{ csrf_field() }}
		      	<div class="modal-header">
			        <h5 class="modal-title">Aggiungi utente</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
					<div class="row">
						<div>
							<input type="hidden" id="input_clients_ids_add_user_modal" name="clients">
						</div>
						<div>
							<input type="hidden" id="input_projects_ids_add_user_modal" name="projects">
						</div>
						<div class="col-lg">
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" required>
						</div>
						<div class="col-lg">
							<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required>
						</div>
						<div class="col-lg">
							<input type="text" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
						</div>
						<div class="col-lg">
							<select id="role" name="is_admin" class="form-control">
								<option value="0">Utente Normale</option>
						  		<option value="1">Amministratore</option>
							</select>
			      		</div>
					</div>
					<div class="row">
						<div class="col-lg mt-2">
							<span>Autorizzazioni</span>
							<ul class="ul_root">
								<?php
								foreach ($clients as $client) {
									echo "
									<li><span class='box' data-type='client' data-id='".$client->id."'>".$client->name."</span>
										<ul class='nested'>";
									$projects = DB::table('projects')->where('id_client', $client->id)->get();
									foreach ($projects as $project) {
										echo "
												<li><span class='box' data-type='project' data-id='".$project->id."'>".$project->name."</span></li>
											";
									}
									echo "
										</ul>
									</li>";
								}
								?>
							</ul>
			      		</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Aggiungi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/update_user">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Modifica utente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
						<div>
							<input type="hidden" id="input_clients_ids_update_user_modal" name="clients">
						</div>
						<div>
							<input type="hidden" id="input_projects_ids_update_user_modal" name="projects">
						</div>
						<div class="col-lg">
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" required>
						</div>
						<div class="col-lg">
							<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required>
						</div>
						<div class="col-lg">
							<input type="text" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
						</div>
						<div class="col-lg">
							<select id="role" name="is_admin" class="form-control">
								<option value="0">Utente Normale</option>
								<option value="1">Amministratore</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg mt-2">
							<span>Autorizzazioni</span>
							<ul class="ul_root">
								<?php
								foreach ($clients as $client) {
									echo "
									<li><span class='box' data-type='client' data-id='".$client->id."'>".$client->name."</span>
										<ul class='nested'>";
									$projects = DB::table('projects')->where('id_client', $client->id)->get();
									foreach ($projects as $project) {
										echo "
												<li><span class='box' data-type='project' data-id='".$project->id."'>".$project->name."</span></li>
											";
									}
									echo "
										</ul>
									</li>";
								}
								?>
							</ul>
			      		</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Modifica</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_user_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/delete_user">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Rimuovere utente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">
							<span id="message"></span>
						</div>
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
					<button type="submit" class="btn btn-primary">Rimuovi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-4">
		<div class="col nopadding">
			<span class="font-weight-bold">
				Utenti
			</span>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col nopadding">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_user_modal">
				<i class="fa fa-plus"></i>
			</button>
			<span>Aggiungi utente</span>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col nopadding">
			<span>Elenco utenti</span>
		</div>
	</div>
	<div class="row">
		<table class="table">
		<thead>
			<tr>
				<th scope="col">Nome</th>
				<th scope="col">Email</th>
				<th scope="col">Ruolo</th>
				<th scope="col"></th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($users as $user) {
			echo "
				<tr>
					<td>".
						$user->name.
					"</td>
					<td>".
						$user->email.
					"</td>
					<td>".
						(($user->is_admin) ? 'Amministratore' : 'Utente Normale').
					"</td>
					<td align='right'>
						<a data-toggle='modal' class='clickable ml-2' data-target='#update_user_modal' data-name='".$user->name."' data-email='".$user->email."' data-role='".$user->is_admin."' data-id='".$user->id."'>
							<i class='fa fa-pencil-alt'></i>
						</a>
						<a data-toggle='modal' class='clickable ml-2' data-target='#delete_user_modal' data-name='".$user->name."' data-id='".$user->id."'>
							<i class='fa fa-trash'></i>
						</a>
					</td>
				</tr>";
		}
		?>
		</tbody>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/users_control.js"></script>

</body>
</html>
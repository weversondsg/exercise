<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Clienti</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/general.css">
</head>
<body>

<div class="modal fade" id="add_client_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/add_client">
				{{ csrf_field() }}
		      	<div class="modal-header">
			        <h5 class="modal-title">Aggiungi cliente</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
					<div class="row">
						<div class="col-lg">
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Aggiungi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="update_client_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/update_client">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Modifica cliente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
						<div class="col-lg">
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Modifica</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_client_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/delete_client">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Rimuovere cliente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">
							<span id="message"></span>
						</div>
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
					<button type="submit" class="btn btn-primary">Rimuovi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-4">
		<div class="col nopadding">
			<span class="font-weight-bold">
				Clienti
			</span>
		</div>
	</div>
	<?php
	if (Auth::user()->is_admin) {
		echo '
		<div class="row mt-4">
			<div class="col nopadding">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_client_modal">
					<i class="fa fa-plus"></i>
				</button>
				<span>Aggiungi cliente</span>
			</div>
		</div>';
	}
	?>
	<div class="row mt-4">
		<div class="col nopadding">
			<span>Elenco clienti</span>
		</div>
	</div>
	<div class="row">
		<table class="table">
		<thead>
			<tr>
				<th scope="col">Nome</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($clients as $client) {
			echo "
				<tr>
					<td>
						<span class='clickable_item clickable' data-id='".$client->id."'>
							".$client->name."
						</span>
					</td>";

			if (Auth::user()->is_admin) {
				echo "
					<td align='right'>
						<a data-toggle='modal' class='clickable ml-2' data-target='#update_client_modal' data-name='".$client->name."' data-id='".$client->id."'>
							<i class='fa fa-pencil-alt'></i>
						</a>
						<a data-toggle='modal' class='clickable ml-2' data-target='#delete_client_modal' data-name='".$client->name."' data-id='".$client->id."'>
							<i class='fa fa-trash'></i>
						</a>
					</td>";
			}
			echo "</tr>";
		}
		?>
		</tbody>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/clients_control.js"></script>

</body>
</html>
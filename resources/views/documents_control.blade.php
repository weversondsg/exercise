<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Documenti</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/general.css">
</head>
<body>

<div class="modal fade" id="add_document_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" enctype="multipart/form-data" action="/add_document">
				{{ csrf_field() }}
		      	<div class="modal-header">
			        <h5 class="modal-title">Aggiungi documento</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
					<div class="row">
						<div>
							<?php
							echo "<input type='hidden' id='id_project' name='id_project' value='".$id_project."'>";
							?>
						</div>
						<div class="col-lg">
							<input type="file" name="fileToUpload" id="fileToUpload">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Aggiungi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="update_document_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="#">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Modifica documento</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
						<div>
							<?php
							echo "<input type='hidden' id='id_project' name='id_project' value='".$id_project."'>";
							?>
						</div>
						<div class="col-lg">
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Modifica</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_document_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/delete_document">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Rimuovere documento</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col">
							<span id="message"></span>
						</div>
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
						<div>
							<?php
							echo "<input type='hidden' id='id_project' name='id_project' value='".$id_project."'>";
							?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
					<button type="submit" class="btn btn-primary">Rimuovi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="update_status_document_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" class="form-signin" action="/update_document">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title">Approvazione documento <span id="document_name"></span></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row" align="center">
						<div>
							<input type="hidden" id="inputId" name="id">
						</div>
						<div>
							<?php
							echo "<input type='hidden' id='id_project' name='id_project' value='".$id_project."'>";
							?>
						</div>
						<div class="col">
							<button type="submit" name="btn" value="rifiutato" class="btn btn-light btn-lg"><i class='fas fa-thumbs-down fa-flip-horizontal'></i></button><br>
							<span>Rifiuta</span>
						</div>
						<div class="col">
							<button type="submit" name="btn" value="approvato" class="btn btn-light btn-lg"><i class='fas fa-thumbs-up'></i></button><br>
							<span>Approva</span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-4">
		<div class="col nopadding">
			<span class="font-weight-bold">
				Documenti
			</span>
		</div>
	</div>
	<?php
	if (Auth::user()->is_admin) {
		echo '
		<div class="row mt-4">
			<div class="col nopadding">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_document_modal">
					<i class="fa fa-plus"></i>
				</button>
				<span>Aggiungi documento</span>
			</div>
		</div>';
	}
	?>
	<div class="row mt-4">
		<div class="col nopadding">
			<span>Elenco documenti</span>
		</div>
	</div>
	<div class="row">
		<table class="table">
		<thead>
			<tr>
				<th scope="col">Nome</th>
				<th scope="col">Stato</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($documents as $document) {
			echo "
				<tr>
					<td>
						<span class='clickable_item clickable' data-name='".$document->name."' data-id='".$document->id."'>
							".$document->name."
						</span>
					</td>
					<td>
						<span>
							".$document->status."
						</span>
					</td>
					<td align='right'>";

			if (Auth::user()->is_admin) {
				echo "
						<a data-toggle='modal' class='clickable ml-2' data-target='#update_document_modal' data-name='".$document->name."' data-id='".$document->id."'>
							<i class='fa fa-pencil-alt'></i>
						</a>
						<a data-toggle='modal' class='clickable ml-2' data-target='#delete_document_modal' data-name='".$document->name."' data-id='".$document->id."'>
							<i class='fa fa-trash'></i>
						</a>";
			}
			echo "		<a data-toggle='modal' class='clickable ml-2' data-target='#update_status_document_modal' data-name='".$document->name."' data-id='".$document->id."'>
							<i class='fas fa-thumbs-up'></i>
						</a>
					</td>
				</tr>";
		}
		?>
		</tbody>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/documents_control.js"></script>

</body>
</html>
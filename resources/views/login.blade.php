<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<link rel="stylesheet" href="http://localhost/laravel/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  	<div class="row mt-5">
  		<div class="col-lg-3"></div>
	    <div class="col-lg-6">
			<form method="post" class="form-signin" action="/validate_login">
				{{ csrf_field() }}
				<h1 class="h3 mb-3 font-weight-normal">Login</h1>
				<label for="inputEmail" class="sr-only">Email</label>
				<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required autofocus>
				<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
				<div class="checkbox mb-3">
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
			</form>
	    </div>
  		<div class="col-lg-3"></div>
  	</div>
</div>

</body>
</html>
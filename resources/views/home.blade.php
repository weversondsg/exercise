<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Home</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-5">
	<div class="jumbotron" align="center">
		<?php
		echo "<h1 class='display-4'>Benvenuto, ".Auth::user()->name."!</h1>";
		?>
		<hr class="my-4">
		<div>
			<a class="btn btn-primary btn-lg" href="/users_control" role="button">Gestisci utenti</a>
			<a class="btn btn-primary btn-lg" href="/clients_control" role="button">Gestisci clienti</a>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>